import React, {Component} from 'react';
import classNames from 'classnames';
import {Header} from './header';
import load from './scripts/loadHelpers';
import Page from './scripts/Page';
import Graph from './scripts/Graph';
import SearchEngine from './scripts/SearchEngine';
import { Search } from './search';

const pages = ['index', 'about', 'content', 'labdescription', 'labdescription2'];
const loadPage = link => {
  return load('GET', `/custom/${link}.cust`)
      .then(text => ({
        link,
        text
      }));
};

const setPage = link => {
  const container = $('#main-container');
  container.empty();
  const text = (window.pages && window.pages[link] && window.pages[link].text) || '';
  if (text) {
    window.currentPage = link;
    const page = new Page(text);
    const pageMarkup = page.getPageMarkup();
    container.append(pageMarkup);
  }
};

const navigateByLink = link => {
  if (link.startsWith('https://') || link.startsWith('http://')) {
    window.open(link, '_blank');
  } else {
    window.location.hash = `#${link}`;
    setPage(link);
  }
};


const loadAllPages = () => {
  const requests = pages.map(page => loadPage(page));
  return Promise.all(requests)
      .then(pages => {
        const obj = {};
        pages.forEach(page => {
          const {link, text} = page;
          const instance = new Page(text);
          obj[link] = {
            text,
            meta: instance.getPageMeta(),
            links: instance.getPageLinks()
          };
        });
        window.pages = obj;
      });
};

export class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searching: false,
      loading: false,
      foundPages: []
    };

    this.engine = null;
    this.search = this.search.bind(this);
    this.searchClean = this.searchClean.bind(this);
  }
  componentDidMount() {
    window.onhashchange = this.onHashChange.bind(this);
    document.addEventListener('click', this.onDocumentClick.bind(this));
    loadAllPages()
      .then(() => {
          this.onHashChange();
          this.engine = new SearchEngine(window.pages);
      });
  }

  onDocumentClick(event) {
    const target = event.target;
    const link = target.getAttribute('data-href');
    if (link) {
      if (this.state.searching) {
        this.header.searchClean();
      }
      navigateByLink(link);
    }
  }

  onHashChange() {
    const hash = window.location.hash.slice(1);
    if (hash !== window.currentPage && hash !== 'search') {
      if (this.state.searching) {
        this.header.searchClean();
      }
      navigateByLink(hash || 'index');
    } else if (hash === 'search' && !this.state.searching) {
      navigateByLink('index');
    }
  }

  graphBtnClick() {
    const linksObj = window.pages;
    const graph = new Graph(pages);
    for (const key in linksObj) {
      if (linksObj[key] && linksObj[key].links) {
        graph.addEdge(key, linksObj[key].links);
      }
    }
    const isCyclic = graph.isCyclic();
    alert(isCyclic ? 'There is link cycle' : 'There are no cycles');
  }

  search(event) {
    if (event.key === 'Enter') {
      window.location.hash = 'search';
      window.currentPage = 'search';
      this.setState({
        searching: true,
        loading: true
      });
      this.engine.getSearchedPages(event.target.value.trim())
        .then((foundPages) => {
          this.setState({
            foundPages,
            loading: false
          });
        })
    }
  }

  searchClean() {
    this.setState({
      searching: false,
      foundPages: []
    });
  }

  render() {
    const { searching, foundPages, loading } = this.state;
    return (
      <div className="container">
        <Header
          search={this.search}
          graphBtnClick={this.graphBtnClick}
          searching={searching}
          searchClean={this.searchClean}
          ref={node => this.header = node}
        />
        { searching
          ? <Search
              foundPages={foundPages}
              loading={loading}
          />
          : ''
        }        
        <div id="main-container" className={classNames('page-content', {
          hidden: searching
        })}/>
      </div>
    );
  }
}
