import React, {Component} from 'react';

export class Header extends Component {
  constructor(props) {
    super(props);

    this.searchClean = this.searchClean.bind(this);
  }

  searchClean(isFocus) {
    this.input.value = '';
    if (isFocus) {
      this.input.focus();
    }
    this.props.searchClean();
  }

  render() {
    const { graphBtnClick, search, searching } = this.props;
    return (
      <header className="header">
        <h3>IIT</h3>

        <div className="actions">
          <input
            id="search-input"
            ref={node => (this.input = node)}
            className="search-input"
            onKeyPress={search}
          />
          { searching
            ? <i className="fas fa-times" onClick={this.searchClean.bind(this, true)}/>
            : <i className="fas fa-search"/>
          }
          <div
            id="graph-calculation"
            className="header-btn"
            onClick={graphBtnClick}>
              Graph Cycle
          </div>
        </div>
      </header>
    );
  }
}
