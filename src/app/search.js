import React, {Component} from 'react';

export class Search extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    const { foundPages, loading } = this.props;
    return (
      <div className="search-results">
        { !loading && foundPages.map((page, index) => (
            <div
                className="search-item"
                key={index}
            >
                <p className="search-title bold" data-href={page.id}>{ page.title }</p>
                <p className="search-description">{ `${page.text}...` } </p>
            </div>
        ))}
        { !foundPages.length && !loading && (
            <div className="no-results">
                Ничего не найдено
            </div>
        )}
      </div>
    );
  }
}
