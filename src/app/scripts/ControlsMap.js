import constants from './constants';

const controlsMap = {
    [constants.FONT]: {
        element: 'div',
        customField: constants.STYLE,
        getStyle: ([ fontFamily ]) => ({
            display: 'inline',
            'font-family': fontFamily || 'Times New Roman'
        }),
        getAttributes: () => ({}),
        className: ''
    },
    [constants.HEADING]: {
        element: 'div',
        getStyle: () => ({
            display: 'block',
            'font-size': '24px'
        }),
        getAttributes: () => ({}),
        className: ''
    },
    [constants.UNDERLINE]: {
        element: 'div',
        getStyle: () => ({
            display: 'inline',
            'text-decoration': 'underline'
        }),
        getAttributes: () => ({}),
        className: ''
    },
    [constants.BOLD]: {
        element: 'div',
        getStyle: () => ({
            display: 'inline',
            'font-weight': 'bold'
        }),
        getAttributes: () => ({}),
        className: ''
    },
    [constants.CURSIVE]: {
        element: 'div',
        getStyle: () => ({
            display: 'inline',
            'font-style': 'italic'
        }),
        getAttributes: () => ({}),
        className: ''
    },
    [constants.FILLED]: {
        element: 'div',
        customField: constants.STYLE,
        getStyle: ([ color ]) => ({
            display: 'inline',
            'background-color': color
        }),
        getAttributes: () => ({}),
        className: ''
    },
    [constants.COLORED]: {
        element: 'div',
        customField: constants.STYLE,
        getStyle: ([ color ]) => ({
            display: 'inline',
            color
        }),
        getAttributes: () => ({}),
        className: ''
    },
    [constants.LINK]: {
        element: 'div',
        customField: constants.ATTRIBUTE,
        getStyle: () => ({
            display: 'inline',
            color: '#310196',
            'cursor': 'pointer'
        }),
        getAttributes: ([ href ]) => ({
            'data-href': href
        }),
        className: 'link-element'
    },
    [constants.LINEBREAK]: {
        element: 'br',
        getStyle: () => ({}),
        getAttributes: () => ({}),
        className: ''
    },
    [constants.UPPERCASE]: {
        element: 'div',
        getStyle: () => ({
            display: 'inline',
            'text-transform': 'uppercase'
        }),
        getAttributes: () => ({}),
        className: ''
    },
    [constants.TEXT]: {
        element: 'div',
        getStyle: () => ({
            display: 'block'
        }),
        getAttributes: () => ({}),
        className: ''
    },
    [constants.FOOTER]: {
        element: 'div',
        getStyle: () => ({
        }),
        getAttributes: () => ({}),
        className: 'page-footer'
    },
    [constants.WRAPPER]: {
        element: 'section',
        customField: constants.ATTRIBUTE,
        getStyle: () => ({
            height: 'calc(100% - 80px)'
        }),
        getAttributes: ([id]) => ({
            id
        }),
        className: 'wrapper'
    },
    [constants.META]: {
        noRender: true,
        getStyle: () => ({}),
        getAttributes: ([]) => ({})
    }
}

export default controlsMap;
