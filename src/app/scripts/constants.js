const FONT = 'Font';
const HEADING = 'Heading';
const UNDERLINE = 'Underline';
const BOLD = 'Bold';
const CURSIVE = 'Cursive';
const FILLED = 'Filled';
const COLORED = 'Colored';
const LINK = 'Link';
const LINEBREAK = 'Linebreak';
const UPPERCASE = 'Uppercase';
const TEXT = 'Text';
const WRAPPER = 'Wrapper';
const FOOTER = 'Footer';
const META = 'Meta';

const ATTRIBUTE = 'ATTRIBUTE';
const STYLE = 'STYLE';

const constants = {
    FONT,
    HEADING,
    BOLD,
    UNDERLINE,
    CURSIVE,
    FILLED,
    COLORED,
    LINEBREAK,
    LINK,
    UPPERCASE,
    TEXT,
    WRAPPER,
    FOOTER,
    META,
    ATTRIBUTE,
    STYLE
};

export default constants;
  
    