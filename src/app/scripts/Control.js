export default class Control {
  constructor({ uuid, tag, element, style, attributes, className, noRender }) {
    this.element = element;
    this.uuid = uuid;
    this.tag = tag;
    this.style = style;
    this.attributes = attributes;
    this.className = className;
    this.children = [];
    this.parent = null;
    this.innerText = '';
    this.noRender = noRender;
  }

  addChild(control) {
    this.children.push(control);
  }

  setParent(control) {
    this.parent = control;
  }

  setInnerText(innerText) {
    this.innerText = innerText;
  }

  render() {
    if (this.noRender) {
      return;
    }
    const control = $(`<${this.element}></${this.element}>`)
      .addClass(this.className);
    for (const key in this.attributes) {
      if (this.attributes[key]) {
        control.attr(key, this.attributes[key]);
      }
    }
    for (const key in this.style) {
      if (this.style[key]) {
        control.css(key, this.style[key]);
      }
    }
    for (let i = 0; i < this.children.length; i++) {
      const child = this.children[i];
      let childControl = '';
      if (typeof child === 'string') {
        childControl = child;
      } else {
        childControl = child.render();
      }
      control.append(childControl);
    }
    return control;
  }
}
