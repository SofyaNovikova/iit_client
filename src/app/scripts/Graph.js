export default class Graph {
    constructor(vertices) {
        this.graph = {};
        this.vertices = vertices;
    }

    addEdge(key, arr) {
        if (!this.graph[key]) {
            this.graph[key] = [];
        }
        this.graph[key] = arr || [];
    }

    getCyclic(v, visited, recStack) {
        visited[v] = true;
        recStack[v] = true;

        for (let i = 0; i < this.graph[v].length; i++) {
            const neighbour = this.graph[v][i];
            if (visited[neighbour] === false) {
                if (this.getCyclic(neighbour, visited, recStack) === true) {
                    return true;
                }
            } else if (recStack[neighbour] === true) {
                return true;
            }
        }

        recStack[v] = false;
        return false;
    }

    isCyclic() {
        const visited = {};
        const recStack = {};
        this.vertices.forEach((vertex) => {
            visited[vertex] = false;
            recStack[vertex] = false;
        })

        for (let i = 0; i < this.vertices.length; i++) {
            const vertex = this.vertices[i];
            if (!visited[vertex]) {
                if (this.getCyclic(vertex, visited, recStack)) {
                    return true;
                }
            }
        }

        return false;

    }
}
