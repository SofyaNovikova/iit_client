import Az from 'az';
import Page from './Page';

import load from './loadHelpers';
import config from '../../config';

const FULL_MATCH_SCORE = 20;
const META_MATCH_SCORE = 10;
const MAIN_WORD_MATCH_SCORE = 5;
const ADDITIONAL_WORD_MATCH_SCORE = 3;
 
export default class SearchEngine {
    constructor(pages) {
        this.tokens = [];
        this.pages = [];

        this.parsePages(pages);
    }

    parsePages(pages) {
        for (let link in pages) {
            if (pages[link] && pages[link].text) {
                const page = new Page(pages[link].text);
                const { id, text } = page.getPageTextContent();
                const title = page.getPageHeadingContent();
                if (id) {
                    this.indexPage(text)
                        .then((index) => {
                            this.pages.push({
                                score: 0,
                                id,
                                text,
                                title,
                                index,
                                meta: pages[link].meta
                            });
                        });
                }
            }
        }
    }

    getResults(text) {
        const tokens = Az.Tokens(text)
            .done();
        const payload = {
            tokens: tokens.reduce((arr, token) => {
                const type = token.type.toString();
                const subType = token.subType && token.subType.toString();
                if (type === 'WORD' && subType === 'CYRIL') {
                    const word = text.slice(token.st, token.st + token.length);
                    arr.push(word);
                }
                return arr;
            }, [])
        }

        return load('POST', `${config.API}morphs`, JSON.stringify(payload))
            .then((response) => {
                const result = JSON.parse(response);
                return result;
            });
    }

    indexPage(text) {
        return this.getResults(text)
            .then((tokens) => {
                return tokens.reduce((arr, token) => {
                    if (this.getTokenWeight(token.tag)) {
                        arr.push(token.norm);
                    }
                    return arr;
                }, []);
            })
    }

    getSearchedPages(text) {
        return this.getResults(text)
            .then((tokens) => {
                this.getPageScore(tokens, text);
                this.pages.sort((a, b) => {
                    if (a.score > b.score) {
                        return -1;
                    }
                    if (a.score < b.score) {
                        return 1;
                    }

                    return 0;
                });
                const foundPages = this.pages
                    .filter(page => page.score > 0)
                    .map(page => ({
                        id: page.id,
                        title: page.title,
                        text: `${page.text.slice(0, 100)}...`
                    }))
                return foundPages;
            })
    }

    getTokenWeight(tag) {
        switch (tag.POS) {
            case 'PREP':
            case 'INTJ':
            case 'CONJ':
                return 0;
            case 'NOUN':
                return MAIN_WORD_MATCH_SCORE;
            case 'INFN':
            case 'VERB':
                return MAIN_WORD_MATCH_SCORE;
            case 'ADJF':
            case 'ADVB':
                return ADDITIONAL_WORD_MATCH_SCORE;
            default:
                return 0;
        }
    }

    getPageScore(tokens, text) {
        this.pages.forEach(page => {
            page.score = 0;
            if (tokens.length > 2) {
                page.score += this.getFullMatchScore(text, page.text);
            }
            tokens.forEach((token) => {
                page.score += this.getMetaMatchScore(token, page.meta);
                page.score += this.getIndexMatchScore(token, page.index);
            });
        });
    }

    getFullMatchScore(_token, _text) {
        let index = 0, startIndex = 0, count = 0;
        const token = _token.toLowerCase();
        const text = _text.toLowerCase();
        while ((index = text.indexOf(token, startIndex)) > -1) {
            count ++;
            startIndex = index + token.length;
        }
        return count * FULL_MATCH_SCORE;        
    }

    getMetaMatchScore(token, meta) {
        if (meta.indexOf(token.norm) > -1) {
            return META_MATCH_SCORE;
        }
        return 0;
    }

    getIndexMatchScore(token, index) {
        let score = 0;
        index.forEach((word) => {
            if (word === token.norm) {
                score += this.getTokenWeight(token.tag);
            }
        });
        return score;
    }
}