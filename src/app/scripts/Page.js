

import Control from './Control';
import ControlsMap from './ControlsMap';
import constants from './constants';

const tagRegex = /\|(\-)?[a-zA-Z\/\s\d\.\#\:\+\-]+(\-)?\|/;
const generateGUID = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
      const r = Math.random() * 16 | 0; // eslint-disable-line no-bitwise
      const v = c === 'x' ? r : (r & 0x3 | 0x8); // eslint-disable-line no-mixed-operators, no-bitwise
      return v.toString(16);
  });
};
export default class Page {
  constructor(pageText) {
    this.pageText = pageText;
    this.controlsTree = { root: null };
    this.test = [];
  }

  parseText() {
    let text = this.pageText.trim();
    let currentControl = '';
    while (text) {
      let word = '';
      const symbol = text[0];
      if (symbol === '|' && !word) {
        word = text.match(tagRegex)[0];
        const parsedWord = word && word.slice(1, -1);
        if (parsedWord && !parsedWord.startsWith('-') && !parsedWord.endsWith('-')) {
          const attrs = parsedWord.split('+');
          const tag = attrs[0];
          const control = this.getControl(tag, attrs.slice(1));
          if (control) {
            this.setControlToTheTree(currentControl, control);
            currentControl = control;
          }
        } else if (parsedWord && parsedWord.startsWith('-')) {
          currentControl = currentControl.parent || this.controlsTree.root;
        } else if (parsedWord && parsedWord.endsWith('-')) {
          const attrs = parsedWord.slice(0, -1).split('+');
          const tag = attrs[0];
          const control = this.getControl(tag, attrs.slice(1));
          if (control) {
            this.setControlToTheTree(currentControl, control);
            currentControl = control.parent;
          }
        }
        if (word) {
          text = text.replace(word, '').trim();
          word = '';
        }
      } else {
        const index = text.indexOf('|');
        const innerText = text.slice(0, index).trim();
        this.setControlToTheTree(currentControl, innerText);
        text = text.replace(innerText, '').trim();
      }
    }
  }

  getControl(tag, attrs) {
    const controlObj = ControlsMap[tag];
    if (controlObj) {
      const { element, className, noRender } = controlObj;
      let style = [], attributes = [];
      style = controlObj.getStyle(attrs);
      attributes = controlObj.getAttributes(attrs);
      const uuid = generateGUID();
      return new Control({ uuid, tag, element, style, attributes, className, noRender });
    }

    return null;
  }

  setControlToTheTree(currentControl, control, isChild) {
    if (!this.controlsTree.root) {
      this.controlsTree.root = control;
      return;
    }
    currentControl.addChild(control);
    if (control instanceof Control) {
      control.setParent(currentControl);
    }
  }

  renderPage() {
    if (this.controlsTree.root) {
      const rootControl = this.controlsTree.root.render();
      return rootControl;
    }

    return '';
  }


  getPageMarkup() {
    this.parseText();
    const control = this.renderPage();
    return control;
  }

  getPageLinks() {
    if (!this.controlsTree.root) {
      this.parseText();
    }
    const root = this.controlsTree.root;
    const links = [];
    if (root) {
      this.goThroughTreeForLinks(root, links);
      return links;
    }
    return [];
  }

  getPageMeta() {
    if (!this.controlsTree.root) {
      this.parseText();
    }
    let meta = [];
    const root = this.controlsTree.root;
    if (root) {
      meta = this.goThroughTreeForMeta(root, meta);
      return meta;
    }
  }

  getPageTextContent() {
    if (!this.controlsTree.root) {
      this.parseText();
    }
    const root = this.controlsTree.root;
    let text = '';
    if (root) {
      const id = root.attributes.id;
      text = this.goThroughTreeForText(root, true, '');
      return { id, text };
    }
    return {};
  }

  getPageHeadingContent() {
    if (!this.controlsTree.root) {
      this.parseText();
    }
    const root = this.controlsTree.root;
    if (root) {
      const heading = this.goThroughTreeForHeading(root);
      const headText = this.goThroughTreeForText(heading, false, '');
      return headText.trim();
    }
    return '';
  }

  goThroughTreeForLinks(element, links) {
    if (element instanceof Control
      && element.tag === constants.LINK && element.attributes['data-href']) {
        const link = element.attributes['data-href'];
        if (!link.startsWith('http') && !link.startsWith('https')) {
          links.push(link);
        }
    }
    if (typeof element === 'string' ||
      element instanceof Control && !element.children.length) {
        return;
    }
    element.children.forEach((child) => {
      this.goThroughTreeForLinks(child, links);
    })
  }

  goThroughTreeForText(element, isFormatting, text) {
    if (typeof element === 'string') {
      let parsed = element;
      if (isFormatting) {
        parsed = parsed.replace(/&nbsp;/g, '');
        parsed = parsed.replace(/[^A-Za-zА-Яа-я\s\d]/g, ' ');
      }
      text += ` ${parsed}`;
      return text;
    } else if (element instanceof Control && (!element.children.length || element.noRender)) {
      return text;
    }
    element.children.forEach((child) => {
      text = this.goThroughTreeForText(child, isFormatting, text);
    });

    return text;
  }

  goThroughTreeForHeading(element, heading) {
    if (element instanceof Control
      && element.tag === constants.HEADING && element.children.length) {
      return element;
    }
    if (typeof element === 'string' ||
      element instanceof Control && !element.children.length) {
      return heading;
    }
    element.children.forEach((child) => {
      heading = this.goThroughTreeForHeading(child, heading);
    })
    return heading;
  }

  goThroughTreeForMeta(element, meta) {
    if (element instanceof Control
      && element.tag === constants.META) {
      const metaChildren = element.children.reduce((arr, child) => {
        if (typeof child === 'string') {
          arr = [...arr, ...child.split(',')];
        }
        return arr;
      }, []);
      meta = [...meta, ...metaChildren];
      return meta;
    }
    if (typeof element === 'string' ||
      element instanceof Control && !element.children.length) {
      return meta;
    }
    element.children.forEach((child) => {
      meta = this.goThroughTreeForMeta(child, meta);
    });
    return meta;
  }
}
