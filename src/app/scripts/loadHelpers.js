
const load = (method, url, payload) => {
    var client = new XMLHttpRequest();

    return new Promise((resolve, reject) => {
        client.onreadystatechange = () => {
            if (client.readyState !== 4) return;
            if (client.status >= 200 && client.status < 300) {
                resolve(client.response);
            } else {
                reject({
                    status: client.status,
                    statusText: client.statusText
                });
            }
        }
        client.open(method, url, true);
        if (method === 'POST') {
            client.setRequestHeader('Content-Type', 'application/json');
        }
        client.send(payload || '');
    })
};

export default load;
